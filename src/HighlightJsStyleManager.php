<?php

namespace Drupal\highlight_js;

use Drupal\Component\Utility\Unicode;

/**
 * Highlight.js style manager.
 */
class HighlightJsStyleManager {

  /**
   * List the available themes.
   */
  public function getStyleList() {
    $directory = 'libraries/highlightjs/styles/';
    $list = [];
    if (!empty($directory)) {
      $files = file_scan_directory($directory, '/.*\.css$/', ['key' => 'name']);
      foreach ($files as $key => $fileinfo) {
        $list[Unicode::strtolower($key)] = Unicode::ucfirst($key);
      }
      natcasesort($list);
    }

    return $list;
  }

}
